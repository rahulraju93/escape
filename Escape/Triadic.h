#ifndef ESCAPE_TRIADIC_H_
#define ESCAPE_TRIADIC_H_

#include "Escape/ErrorCode.h"
#include "Escape/Graph.h"
#include "Escape/Digraph.h"
#include <iostream>
#include <math.h>

using namespace Escape;

// Structure that stores triangle information of graph

struct TriangleInfo
{
    Count total; // total number of triangles
    Count *perVertex;  // array storing number of triangles for each vertex
    Count *perEdge;   // arry storing number of triangles for each edge

};

struct DirectedTriangleInfo 
{
    Count Acyclic;    //total number of Acyclic triangles
    Count Cycle;      //total number of Cycle triangles
    Count Out_Plus;       //total number of Out+ triangles
    Count Cycle_Plus;     //total number of Cycle+ triangles
    Count In_Plus;        //total number of In+ triangles
    Count Cycle_Plus_Plus;    //total number of Cycle++ triangles
    Count Reciprocal; //total number of Reciprocal triangles
    Count total_directed_triangles; //sum of all the above triangles
    void print(DirectedTriangleInfo ret);
};

void DirectedTriangleInfo::print(DirectedTriangleInfo ret)
{ 
  std::cout << "===========================" <<"\n";
  std::cout << "Counting Directed Triangles" <<"\n";
  std::cout << "===========================" << "\n" << "\n";;
  std::cout << "Acyclic Triangles = " << ret.Acyclic <<"\n";
  std::cout << "Cycle Triangles = " << ret.Cycle <<"\n";
  std::cout << "Out_Plus Triangles = " << ret.Out_Plus <<"\n";
  std::cout << "Cycle_Plus Triangles = " << ret.Cycle_Plus <<"\n";
  std::cout << "In_Plus Triangles = " << ret.In_Plus <<"\n";
  std::cout << "Cycle_Plus_Plus Triangles = " << ret.Cycle_Plus_Plus <<"\n";
  std::cout << "Reciprocal Triangles = " << ret.Reciprocal <<"\n";
  std::cout << "\n";
  std::cout << "Total Directed Triangles = " << ret.total_directed_triangles <<"\n";
  std::cout << "\n";
}
   
//use with a Graph or CGraph as argument
template <class T>
TriangleInfo newTriangleInfo(const T* graph)
{
  TriangleInfo ret;
  ret.total = 0;
  ret.perVertex = new Count[graph->nVertices];
  ret.perEdge = new Count[graph->nEdges];
  return ret;
}


void delTriangleInfo(TriangleInfo& info)
{
  delete[] info.perVertex;
  delete[] info.perEdge;
}


// Structure for storing all the triangles of graph
// Interpretation of the structure requires knowledge of the exact CGraph cg used to construct it
// Every index pos in the nbors list of cg corresponds to some edge (i,j). Note that each undirected edge appears as (i,j) and (j,i)
// Then trioffsets[pos] contains the starting index (in *triangles) of all the triangles that (i,j) is incident to.
// Basically, the portion of the array trioffsets[pos] to trioffsets[pos+1] in triangles is a list of vertices v1, v2,... such 
//that each vi forms a triangles with (i,j)
//
// We only store triangles for one copy of edge (i,j), where i < j in the degree ordering.


struct TriangleList
{
    VertexIdx total;
    VertexIdx *triangles;
    EdgeIdx *trioffsets;
};

// The wedge enumeration algorithm that produces all triangles
// Input: a pointer g to a CGraph, that CAN be a DAG. Indeed, we will call wedgeEnumerator on DAGs.
// Output: a TriangleInfo for g. The ordering of edges in perEdge (of TriangleInfo) is that same as g.

TriangleInfo wedgeEnumerator(CGraph *g)
{
   TriangleInfo ret;   // output 
   ret.total = 0;      // initialize outout
   ret.perVertex = new EdgeIdx[g->nVertices+1];
   ret.perEdge = new EdgeIdx[g->nEdges+1]; 

   for (VertexIdx i=0; i < g->nVertices; ++i)
       ret.perVertex[i] = 0;

   for (EdgeIdx j=0; j < g->nEdges; ++j)
       ret.perEdge[j] = 0; 

   VertexIdx end1, end2;
   EdgeIdx loc1, loc2;

   for (VertexIdx i=0; i < g->nVertices; ++i) // loop over vertices
       for (EdgeIdx j = g->offsets[i]; j < g->offsets[i+1]; ++j)   // loop over neighbor of i
           for (EdgeIdx k = j+1; k < g->offsets[i+1]; ++k)         // loop over another neighbor of i
           {
               end1 = g->nbors[j];     // we are now looking at wedge (i, end1, end2), centered at i
               end2 = g->nbors[k];

               loc1 = g->isEdge(end1,end2);  // check if edge (end1, end2) is present
               loc2 = g->isEdge(end2,end1);  // check if (end2, end1) is present. Could be either, since g is not necessarily undirected
               if (loc1 != -1)        // (end1, end2) is present
               {
                   ret.total++;       // found a triangle! So update total.

                   ret.perVertex[i]++; // update all per vertex counts
                   ret.perVertex[end1]++;
                   ret.perVertex[end2]++;

                   ret.perEdge[j]++;  // update all per edge counts. Note that location used is same as position in g->nbors
                   ret.perEdge[k]++;
                   ret.perEdge[loc1]++;
               }
               if (loc2 != -1)       // (end2, end1) is present
               {
                   ret.total++;      // found a triangle! Update total.

                   ret.perVertex[i]++; // update all per vertex counts
                   ret.perVertex[end1]++;
                   ret.perVertex[end2]++;

                   ret.perEdge[j]++;  // update all per edge counts
                   ret.perEdge[k]++;
                   ret.perEdge[loc2]++;
               }
           }

   return ret;
}


// This wedge enumeration algorithm produces all triangles, and is more
// efficient than the previous version. We use binary search to find edges, and also pass
// the original graph to cut down on queries.
// Input: a pointer gout to a CGraph labeled according to degree
// Output: a TriangleInfo for g. The ordering of edges in perEdge (of TriangleInfo) is that same as g.

TriangleInfo betterWedgeEnumerator(CGraph *gout)
{
   TriangleInfo ret;   // output 
   ret.total = 0;      // initialize outout
   ret.perVertex = new EdgeIdx[gout->nVertices+1];
   ret.perEdge = new EdgeIdx[gout->nEdges+1]; 

   for (VertexIdx i=0; i < gout->nVertices; ++i)
       ret.perVertex[i] = 0;

   for (EdgeIdx j=0; j < gout->nEdges; ++j)
       ret.perEdge[j] = 0; 

   VertexIdx end1, end2;
   EdgeIdx loc;

   for (VertexIdx i=0; i < gout->nVertices; ++i) // loop over vertices
       for (EdgeIdx j = gout->offsets[i]; j < gout->offsets[i+1]; ++j)   // loop over neighbor of i
           for (EdgeIdx k = j+1; k < gout->offsets[i+1]; ++k)         // loop over another neighbor of i
           {
               end1 = gout->nbors[j];     // we are now looking at wedge (i, end1, end2), centered at i
               end2 = gout->nbors[k];

               // note that end1 < end2 because of the labeled ordering

               loc = gout->getEdgeBinary(end1,end2);
               if (loc != -1)        // (end1, end2) is present
               {
                   ret.total++;       // found a triangle! So update total.

                   ret.perVertex[i]++; // update all per vertex counts
                   ret.perVertex[end1]++;
                   ret.perVertex[end2]++;

                   ret.perEdge[j]++;  // update all per edge counts. Note that location used is same as position in g->nbors
                   ret.perEdge[k]++;
                   ret.perEdge[loc]++;
               }
           }

   return ret;
}


// This wedge enumeration algorithm produces all directed triangles, and is almost as
// efficient than the undirected version since all the lookups are in O(logN) amortized time. 
//The algorithm counts the number of directed triangles by looking a edges i, end1 and end2. If the pair exists in the
//map storing information for directed and reciprocal edges, then the two neihbouring edges are checked as well to count 
//the number of directed triangles.   
// Input: a pointer gout to a sorted CGraph
// Output: a DirectedTriangleInfo for CGraph listing counts of all 7 types of directed triangles for each vertex
//Note: The original format is preserved so that no changes to subgraph_counts.py is necessary for simplicity and to avoid confusion.
 
DirectedTriangleInfo betterWedgeEnumeratorDirected(CGraph *gout, std::map <std::pair<EdgeIdx,EdgeIdx>, int>  store_direction)
{
    DirectedTriangleInfo ret;   // output for directed traingle information 
    ret.Acyclic = 0;
    ret.Cycle = 0;
    ret.Out_Plus = 0;
    ret.Cycle_Plus = 0;
    ret.In_Plus = 0;
    ret.Cycle_Plus_Plus = 0;
    ret.Reciprocal = 0;


   VertexIdx end1, end2; //vertices that checked as well since a triangle has 3 edges

   for (VertexIdx i=0; i < gout->nVertices; ++i) // loop over vertices
       for (EdgeIdx j = gout->offsets[i]; j < gout->offsets[i+1]; ++j)   // loop over neighbor of i
           for (EdgeIdx k = j+1; k < gout->offsets[i+1]; ++k)         // loop over another neighbor of i
           {
               end1 = gout->nbors[j];     // we are now looking at wedge (i, end1, end2), centered at i
               end2 = gout->nbors[k]; 
               
               //checking for acyclic triangles here and since the edge end1, end2 can point in either direction both are taken into account
               if(store_direction.find({i, end1}) != store_direction.end() && store_direction[{i, end1}] == 1)
                  if(store_direction.find({i, end2}) != store_direction.end() && store_direction[{i, end2}] == 1)
                    if((store_direction.find({end1, end2}) != store_direction.end() && store_direction[{end1, end2}] == 1) 
                    || (store_direction.find({end2, end1}) != store_direction.end() && store_direction[{end2, end1}] == 1))
                       ret.Acyclic++;

		//checking for cycle triangles in the directed graph, since there are two ways to traverse a cycle both are implemented               
		if(store_direction.find({i, end1}) != store_direction.end() && store_direction[{i, end1}] == 1)
                  if(store_direction.find({end1, end2}) != store_direction.end() && store_direction[{end1, end2}] == 1)
                    if(store_direction.find({end2, i}) != store_direction.end() && store_direction[{end2, i}] == 1)
                       ret.Cycle++;
		
		//checking the opposite direction for each vertex centered at i	
                if(store_direction.find({i, end2}) != store_direction.end() && store_direction[{i, end2}] == 1)
                  if(store_direction.find({end2, end1}) != store_direction.end() && store_direction[{end2, end1}] == 1)
                    if(store_direction.find({end1, i}) != store_direction.end() && store_direction[{end1, i}] == 1)
                       ret.Cycle++;     
		
	       //checking for Out+ triangles by accounting for an edge with reciprocity
	       //Note that for each centered vertex i, it counts the Cycle+ triangles thus allowing for all possible combinations of Out+ triangles that are existing in the CGraph 	
               if(store_direction.find({i, end1}) != store_direction.end() && store_direction[{i, end1}] == 1)
                  if(store_direction.find({i, end2}) != store_direction.end() && store_direction[{i, end2}] == 1)
                    if(store_direction.find({end1, end2}) != store_direction.end() && store_direction[{end1, end2}] == 2)
                       ret.Out_Plus++;
                       
		//checking for both directions of the Cycle+ traversal   
                if(store_direction.find({i, end1}) != store_direction.end() && store_direction[{i, end1}] == 1)
                  if(store_direction.find({end1, end2}) != store_direction.end() && store_direction[{end1, end2}] == 2)
                    if(store_direction.find({end2, i}) != store_direction.end() && store_direction[{end2, i}] == 1)
                       ret.Cycle_Plus++;
		
		//opposite direction check
                if(store_direction.find({i, end2}) != store_direction.end() && store_direction[{i, end2}] == 1)
                  if(store_direction.find({end1, end2}) != store_direction.end() && store_direction[{end1, end2}] == 2)
                    if(store_direction.find({end1, i}) != store_direction.end() && store_direction[{end1, i}] == 1)
                       ret.Cycle_Plus++;     
		
		//check for the edges pointing to i and for an edge with reciprocity
                if(store_direction.find({end1, i}) != store_direction.end() && store_direction[{end1, i}] == 1)
                  if(store_direction.find({end2, i}) != store_direction.end() && store_direction[{end2, i}] == 1)
                    if(store_direction.find({end1, end2}) != store_direction.end() && store_direction[{end1, end2}] == 2)
                       ret.In_Plus++;

		//checking both directions for Cycle++ triangles
                //Note that in the above cases, only (end1,end2) are checked for reciprocity but not in this case, hence the extra functionality
                if(store_direction.find({i, end1}) != store_direction.end() && store_direction[{i, end1}] == 1)
                  if(store_direction.find({end1, end2}) != store_direction.end() && store_direction[{end1, end2}] == 2)
		   {  
              	   if(i < end2)
			{	                    
			   if(store_direction.find({i, end2}) != store_direction.end() && store_direction[{i, end2}] == 2)
			   ret.Cycle_Plus_Plus++;
                        }
                    else if(store_direction.find({end2, i}) != store_direction.end() && store_direction[{end2, i}] == 2)
			{   
			   ret.Cycle_Plus_Plus++;   
		    	}
		   }
                       
		//checking opposite direction
                if(store_direction.find({i, end2}) != store_direction.end() && store_direction[{i, end2}] == 1)
                  if(store_direction.find({end1, end2}) != store_direction.end() && store_direction[{end1, end2}] == 2)
		   {  
              	   if(i < end1)
			{	                    
			   if(store_direction.find({i, end1}) != store_direction.end() && store_direction[{i, end1}] == 2)
			   ret.Cycle_Plus_Plus++;
                        }
                    else if(store_direction.find({end1, i}) != store_direction.end() && store_direction[{end1, i}] == 2)
			{   
			   ret.Cycle_Plus_Plus++;   
		    	}
		   }

		//checking whether all existing edges are reciprocal
                 if(store_direction.find({i, end1}) != store_direction.end() && store_direction[{i, end1}] == 2)
                  if(store_direction.find({i, end2}) != store_direction.end() && store_direction[{i, end2}] == 2)
                    if(store_direction.find({end1, end2}) != store_direction.end() && store_direction[{end1, end2}] == 2)
                       ret.Reciprocal++;           
           }
   
    
   ret.Cycle = ret.Cycle/3; 			//since we count cycles for each of the vertexes in a triangle we divide by 3 
   ret.Cycle_Plus_Plus = ret.Cycle_Plus_Plus/2; //since the same edge repeats twice in the case of Cycle++ we divide by 2
   ret.Reciprocal = ret.Reciprocal/3; 	        //since we count reciprocal triangles for each of the vertexes in a triangle we divide by 3
   ret.total_directed_triangles = ret.Cycle + ret.Out_Plus + ret.Acyclic + ret.Cycle_Plus_Plus + ret.Cycle_Plus + ret.Reciprocal + ret.In_Plus;                                    //total number of directed triangles
return ret;
}


// This assumes that gout and gin are *reverses* of each other
// Given the triangle info with respect to a DAG gout, this outputs the triangle info with respect to the reversed DAG gin.

TriangleInfo moveOutToIn(CGraph *gout, CGraph *gin, TriangleInfo *outinfo)
{
    TriangleInfo ret;
    VertexIdx i,j;
    EdgeIdx loc;
    ret.perVertex = new EdgeIdx[gout->nVertices+1];
    ret.perEdge = new EdgeIdx[gout->nEdges+1]; 
 
    ret.total = outinfo -> total; // total triangles the same 

    for (i=0; i < gout->nVertices; ++i)
    {
        ret.perVertex[i] = outinfo->perVertex[i]; // vertex triangle count the same
        for (EdgeIdx pos = gout->offsets[i]; pos < gout->offsets[i+1]; ++pos)
        {
            j = gout->nbors[pos]; // look at edge i->j
            loc = gin->getEdgeBinary(j,i);

            if (loc == -1) // Edge (j,i) not in gin, so arguments are not reverses of each other
            {
                printf("Error in moveOutToIn: gout and gin not reverses of each other\n");
                printf("i = %ld, j = %ld\n",i,j);
                printf("%ld: ",i);
                for (EdgeIdx posnew = gout->offsets[i]; posnew < gout->offsets[i+1]; posnew++)
                    printf("%ld ",gout->nbors[posnew]);
                printf("\n");
                printf("%ld: ",j);
                for (EdgeIdx posnew = gin->offsets[j]; posnew < gin->offsets[j+1]; posnew++)
                    printf("%ld ",gin->nbors[posnew]);
                printf("\n");
                exit(EXIT_FAILURE);
            }
            ret.perEdge[loc] = outinfo->perEdge[pos]; // loc is index in gin for edge (j,i), so we assign the value of triangle count for (i,j)
        }
    }

    return ret;
}


// This stores all triangles in a TriangleList structure, corresponding to CGraph g. 
// The number of triangles numtri is required for initialization

TriangleList storeAllTriangles(CGraph *g, EdgeIdx numtri)
{
    TriangleList ret;

    ret.total = 3*numtri;
    ret.triangles = new VertexIdx[3*numtri+1];
    ret.trioffsets = new EdgeIdx[g->nEdges+1];

    EdgeIdx posj = 0;

    EdgeIdx current = 0;
    for (VertexIdx i=0; i < g->nVertices; i++)
    {
        VertexIdx degi = g->offsets[i+1] - g->offsets[i];
        for (posj = g->offsets[i]; posj < g->offsets[i+1]; posj++)
        {
            VertexIdx j = g->nbors[posj];
//             if (i == g->nVertices-1)
//                 printf("j is %ld\n",j);
            VertexIdx degj = g->offsets[j+1] - g->offsets[j];
            ret.trioffsets[posj] = current;
            if (degj < degi || (degj == degi && j <= i))
                continue;
            
            for (EdgeIdx ptr = g->offsets[i]; ptr < g->offsets[i+1]; ptr++)
            {
                VertexIdx nbr = g->nbors[ptr];
                if (g->getEdgeBinary(nbr,j) != -1)
                {
//                     if (i == g->nVertices-1)
//                         printf("%ld at ptr %ld: nbr is %ld\n",current,ptr,nbr);
                    ret.triangles[current] = nbr;
                    current++;
                }
            }
        }
    }
    
    ret.trioffsets[posj] = current; // posj is the index after the last edge in g. we set this final offset to the current position

    return ret;
}

// c-triangle-pruning is achieved by removingevery edge that participates in less than c triangles
// Input: CGraph outDAG gout, a value c
// Output: CGraph representation of the c-triangle-pruned graph

CGraph cTriPrune(CGraph *gout, VertexIdx c)
{
    CGraph ret;    // final return value
    Graph retEdges;  // initially, we'll construct truss as list of edges

    retEdges.nVertices = gout->nVertices;
    retEdges.nEdges = 0;
    retEdges.srcs = new VertexIdx[2*gout->nEdges+1];
    retEdges.dsts = new VertexIdx[2*gout->nEdges+1];
    TriangleInfo info = betterWedgeEnumerator(gout);

    for (VertexIdx i=0; i < gout->nVertices; i++)
        for (EdgeIdx posj=gout->offsets[i]; posj < gout->offsets[i+1]; posj++) //looping over all edges
        {
            VertexIdx j = gout->nbors[posj]; // edge (i,j)
            if(info.perEdge[posj] >= c) // edge (i,j) has more than c triangles
            {
               retEdges.srcs[retEdges.nEdges] = i; // insert edge (i,j) in retEdges
               retEdges.dsts[retEdges.nEdges] = j;
               retEdges.nEdges++;

               retEdges.srcs[retEdges.nEdges] = j; // insert edge (j,i) in retEdges
               retEdges.dsts[retEdges.nEdges] = i;
               retEdges.nEdges++;
            }
        }

   ret = makeCSR(retEdges);
   return ret;
}


// A c-truss is the largest subgraph where every edge participates in at least c triangles
// Input: CGraph g, the associated TriangleList tlist, a value c
// Output: CGraph representation of the c-truss

CGraph cTruss(CGraph *g, TriangleList* tlist, VertexIdx c)
{
    CGraph ret;    // final return value
    Graph retEdges;  // initially, we'll construct truss as list of edges

    EdgeIdx *triCount = new EdgeIdx[g->nEdges+1];  // store triangle count of each edge
    EdgeInfo *toDelete = new EdgeInfo[g->nEdges+1]; // store list of edges to be deleted

    EdgeIdx ind_toDelete = 0; // largest index in toDelete

    bool *deleted = new bool[g->nEdges+1];  // store flags for deleted edges

    for (EdgeIdx i=0; i < g->nEdges+1; i++) // initialize all edges as not deleted
        deleted[i] = false;

    for (VertexIdx i=0; i < g->nVertices; i++)
        for (EdgeIdx posj=g->offsets[i]; posj < g->offsets[i+1]; posj++) //looping over all edges
        {
            VertexIdx j = g->nbors[posj]; // edge (i,j)
            if (i > j)   // only look at pairs ordered properly
                continue;
            triCount[posj] = tlist->trioffsets[posj+1] - tlist->trioffsets[posj];  // store the number of triangles that edge (i,j) participates in. Information is exactly stored in tlist
//             printf("%ld %ld %ld\n",i,j,triCount[posj]);
            if (triCount[posj] < c) // edge should be deleted
            {
                EdgeInfo current;
                current.src = i;
                current.dest = j;
                current.index = posj;
                toDelete[ind_toDelete] = current;   // storing current edge in toDelete
                ind_toDelete++;  // update index
            }
        }

    while(ind_toDelete >= 1) // while toDelete is non-empty
    {
        EdgeInfo toRemove = toDelete[ind_toDelete-1]; // get edge to remove
        ind_toDelete--; // update last index in toDelete
        VertexIdx i = toRemove.src;
        VertexIdx j = toRemove.dest;
        deleted[toRemove.index] = true;
       
//         printf("%ld %ld\n",i,j); 
        for (EdgeIdx indk = tlist->trioffsets[toRemove.index]; indk < tlist->trioffsets[toRemove.index+1]; indk++) // looping over triangles that edge participates in
        {
            VertexIdx k = tlist->triangles[indk]; // (i,j,k) forms triangle

            EdgeIdx locik, locjk;
            if (i < k) // get position of ordered edge (i,k)
                locik = g->getEdgeBinary(i,k);
            else
                locik = g->getEdgeBinary(k,i);

            if (locik == -1) // something is wrong. (i,k) has to be edge in g
            {
                printf("Error: edge i,k is not present in graph, but triangle (i,j,k) is stored in tlist\n");
                exit(1);
            }
            if (deleted[locik])  // if (i,k) has been already deleted, then this triangle has been deleted, so continue
                continue;

            if (j < k) // get position of ordered edge (j,k)
                locjk = g->getEdgeBinary(j,k);
            else
                locjk = g->getEdgeBinary(k,j);

            if (locjk == -1) // something is wrong. (j,k) must be edge in g
            {
                printf("Error: edge j,k not present in graph, but triangle (i,j,k) is stored in tlist\n");
                exit(1);
            }
            if (deleted[locjk]) // if (j,k) has been deleted, triangle (i,j,k) has been deleted, so continue
                continue;


            // we delete triangle (i,j,k), so decrement triangle counts appropriately
            triCount[locik]--; // decrement count for (i,k)
            if (triCount[locik] < c) // (i,k) now participates in less than c triangles
            {
                EdgeInfo next = {i,k,locik}; // we should delete (i,k)
                toDelete[ind_toDelete] = next;
                ind_toDelete++;
            }
            triCount[locjk]--; // decrement count for (j,k)
            if (triCount[locjk] < c) // (j,k) now participates in less than c triangles
            {
                EdgeInfo next = {j,k,locjk}; // we should delete (j,k)
                toDelete[ind_toDelete] = next;
                ind_toDelete++;
            }
        }
    }
   // now construct graph with non-deleted edges

   retEdges.nVertices = g->nVertices;
   retEdges.nEdges = 0;
   retEdges.srcs = new VertexIdx[g->nEdges+1];
   retEdges.dsts = new VertexIdx[g->nEdges+1];
   for (VertexIdx i = 0; i < g->nVertices; i++)
       for (EdgeIdx posj = g->offsets[i]; posj < g->offsets[i+1]; posj++) // loop through edges in g
       {
           VertexIdx j = g->nbors[posj];
           if (i < j && !deleted[posj]) // if (i,j) is ordered and not deleted
           {
               retEdges.srcs[retEdges.nEdges] = i; // insert edge (i,j) in retEdges
               retEdges.dsts[retEdges.nEdges] = j;
               retEdges.nEdges++;

               retEdges.srcs[retEdges.nEdges] = j; // insert edge (j,i) in retEdges
               retEdges.dsts[retEdges.nEdges] = i;
               retEdges.nEdges++;
           }
       }

   ret = makeCSR(retEdges);
   return ret;
}

// This function computes closure rate as a function of common neighbors. Consider all pairs (i,j) that
//have exactly c neighbors in common. The closure rate for c is the fraction of such pairs that are also edges.
// Input: CGraph g, array common, array closed
// Output: Final length of arrays common and closed. These arrays are populated with desired output. ith element of common is the number of pairs of vertices that have i neighbors in common.
// The ith element of closed is the number of such pairs that are also edges. 

Count cClosure(CGraph* g, Count* common, Count* closed)
{
    Count ret = 0; // this will eventually be the maximum number of common neighbors
    VertexIdx *wedge_count = new VertexIdx[g->nVertices+1];

    for (VertexIdx i=0; i < g->nVertices; i++) // initialize arrays to 0
    {
        common[i] = 0;
        closed[i] = 0;
        wedge_count[i] = 0;
    }

    for (VertexIdx i=0; i < g->nVertices; i++) // loop over vertices
    {
        for (EdgeIdx posj = g->offsets[i]; posj < g->offsets[i+1]; posj++) // loop over neighbors of i
        {
            VertexIdx j = g->nbors[posj];
            for (EdgeIdx posk = g->offsets[j]; posk < g->offsets[j+1]; posk++)
            {
                VertexIdx k = g->nbors[posk]; // i-j-k is wedge
                if (k <= i) //k is lower in order, so ignore to prevent double counting
                    continue;
                wedge_count[k]++; // update number of wedges ending at k
            }
        }

        for (EdgeIdx posj = g->offsets[i]; posj < g->offsets[i+1]; posj++) // loop over neighbors of i
        {
            VertexIdx j = g->nbors[posj];
            for (EdgeIdx posk = g->offsets[j]; posk < g->offsets[j+1]; posk++)
            {
                VertexIdx k = g->nbors[posk]; // i-j-k is wedge
                if (k <= i) //k is lower in order, so ignore to prevent double counting
                    continue;
                if (wedge_count[k] != 0) // we have not processed (i,k)
                {
                    common[wedge_count[k]]++; // there are exactly wedge_count[k] common neighbors between i and k
                    if (g->isEdgeBinary(i,k)) // (i,k) is edge, so this pair is closed
                        closed[wedge_count[k]]++; // update closed count
                    if (wedge_count[k] > ret)
                        ret = wedge_count[k]; // update the maximum number of common neighbors
                    wedge_count[k] = 0; // reset
                }
            }
        }
    }

    return ret;
}

// Debug function. Prints out data in a TriangleInfo structure. Uses CGraph g to enumerate properly.

void printTri(FILE* f, TriangleInfo *info, CGraph *g)
{
    for (VertexIdx i=0; i < g->nVertices; ++i)
        fprintf(f, "%ld: %ld\n",i,info->perVertex[i]);
    printf("---------------------\n");
    for (VertexIdx i=0; i < g->nVertices; ++i)
        for (EdgeIdx j=g->offsets[i]; j < g->offsets[i+1]; ++j)
            fprintf(f, "(%ld, %ld): %ld\n",i,g->nbors[j],info->perEdge[j]);
}


#endif



