#include "Escape/GraphIO.h"
#include "Escape/EdgeHash.h"
#include "Escape/Digraph.h"
#include "Escape/Triadic.h"
#include "Escape/Graph.h"
#include "Escape/GetAllCounts.h"

using namespace Escape;

int main(int argc, char *argv[])
{
  Graph g;
  if (loadGraph(argv[1], g, 1, IOFormat::escape))
    exit(1);

  std::map <std::pair<EdgeIdx,EdgeIdx>, int> store_direction_ = store_directions(g);

  printf("Loaded graph\n");
  CGraph cg = makeCSR(g);
  cg.sortById();

  printf("Converted to CSR\n");

  DirectedTriangleInfo ret = betterWedgeEnumeratorDirected(&cg, store_direction_);

  printf("Relabeling graph\n");

  CGraph cg_relabel = cg.renameByDegreeOrder();
  cg_relabel.sortById();
  printf("Creating DAG\n");
  CDAG dag = degreeOrdered(&cg_relabel);

  (dag.outlist).sortById();
  (dag.inlist).sortById();

  double nonInd[4];

  getAllThree(&cg_relabel, &dag, nonInd);

  FILE* f = fopen("out.txt","w");
  if (!f)
  {
      printf("could not write to output to out.txt\n");
      return 0;
  }
  fprintf(f,"%ld\n",cg.nVertices);
  fprintf(f,"%ld\n",cg.nEdges);
  for(int i = 0; i < 4; i++)
  {
      fprintf(f,"%f\n",nonInd[i]);
  }

  fclose(f);

ret.print(ret);
}
